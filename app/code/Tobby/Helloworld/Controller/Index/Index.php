<?php

namespace Tobby\Helloworld\Controller\Index;

use Magento\Framework\App\ActionInterface;

class Index implements ActionInterface
{
    public function execute()
    {
        echo 'Execute Action Index_Index OK';
        die();
    }
}
