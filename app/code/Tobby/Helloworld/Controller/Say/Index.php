<?php
namespace Tobby\Helloworld\Controller\Say;

use Magento\Framework\App\ActionInterface;

class Index implements ActionInterface
{
    public function execute()
    {
        echo 'Execute Action Say_Index OK';
        die();
    }
}
