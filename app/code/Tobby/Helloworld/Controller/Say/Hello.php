<?php
namespace Tobby\Helloworld\Controller\Say;

use Magento\Framework\App\ActionInterface;

class Hello implements ActionInterface
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
