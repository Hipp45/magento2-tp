<?php
namespace Tobby\Job\Controller\Adminhtml\Department;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Tobby\Job\Model\DepartmentFactory;
use Tobby\Job\Model\ResourceModel\Department as DepartmentResource;

class Save extends Action
{
    /**
     * @var DepartmentFactory
     */
    protected $_model;
    /**
     * @var DepartmentResource;
     */
    protected $_modelResource;
    /**
     * @param Action\Context $context
     * @param DepartmentFactory $model
     * @param DepartmentResource $modelResource
     */
    public function __construct(
        Action\Context $context,
        DepartmentFactory $model,
        DepartmentResource $modelResource
    ) {
        $this->_model = $model;
        $this->_modelResource = $modelResource;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tobby_Job::department_save');
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue()['department'];
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_model;
//            $model = $this->_objectManager->create(Department::class);
            $id = $this->getRequest()->getParam('id');
//            var_dump($id);
            if ($id) {
                $model->load($id);
//                $this->_modelResource->load($model, $id);
                var_dump($model->getData());
            }

            $newModel = $model->create()->setData($data);
            $this->_eventManager->dispatch(
                'jobs_department_prepare_save',
                ['department' => $newModel, 'request' => $this->getRequest()]
            );

            try {
//                $model->save();
                $newModel->save();
//                $this->_modelResource->save($model);
                $this->messageManager->addSuccessMessage(__('Department saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the department'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
