<?php
namespace Tobby\Job\Controller\Adminhtml\Department;

use Magento\Backend\App\Action;
use Tobby\Job\Model\Department as Department;

class Delete extends Action
{
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tobby_Job::department_delete');
    }
    /**
     * Delete action
     *
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if (!($department = $this->_objectManager->create(Department::class)->load($id))) {
            $this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }
        try {
            $department->delete();
            $this->messageManager->addSuccessMessage(__('Your department has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Error while trying to delete department: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
    }
}
