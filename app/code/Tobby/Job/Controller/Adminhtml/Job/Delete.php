<?php
namespace Tobby\Job\Controller\Adminhtml\Job;

use Magento\Backend\App\Action;
use Tobby\Job\Model\Job as Job;

class Delete extends Action
{
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tobby_Job::job_delete');
    }
    /**
     * Delete action
     *
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if (!($job = $this->_objectManager->create(Job::class)->load($id))) {
            $this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }
        try {
            $job->delete();
            $this->messageManager->addSuccessMessage(__('Your job has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Error while trying to delete job: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['_current' => true]);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', ['_current' => true]);
    }
}
