<?php
namespace Tobby\Job\Controller\Adminhtml\Job;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Tobby\Job\Model\JobFactory;
use Tobby\Job\Model\ResourceModel\Job as JobResource;

class Save extends Action
{
    /**
     * @var JobFactory
     */
    protected $_model;
    /**
     * @var JobResource;
     */
    protected $_modelResource;
    /**
     * @param Action\Context $context
     * @param JobFactory $model
     * @param JobResource $modelResource
     */
    public function __construct(
        Action\Context $context,
        JobFactory $model,
        JobResource $modelResource
    ) {
        $this->_model = $model;
        $this->_modelResource = $modelResource;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tobby_Job::job_save');
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue()['job'];
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_model;
//            $model = $this->_objectManager->create(Department::class);
            $id = $this->getRequest()->getParam('id');
//            var_dump($id);
            if ($id) {
                $model->load($id);
//                $this->_modelResource->load($model, $id);
            }

            $newModel = $model->create()->setData($data);
            $this->_eventManager->dispatch(
                'jobs_job_prepare_save',
                ['job' => $newModel, 'request' => $this->getRequest()]
            );

            try {
//                $model->save();
//                var_dump($newModel->getData());
                $newModel->save();
//                $this->_modelResource->save($model);
                $this->messageManager->addSuccessMessage(__('Job saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the job'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['job_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
