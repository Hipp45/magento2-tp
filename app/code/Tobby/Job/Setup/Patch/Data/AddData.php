<?php
namespace Tobby\Job\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Tobby\Job\Model\Department;

class AddData implements DataPatchInterface, PatchVersionInterface
{
    private $department;

    public function __construct(Department $my_department)
    {
        $this->department = $my_department;
    }

    public static function getDependencies()
    {
        // TODO: Implement getDependencies() method.
        return [];
    }

    public function getAliases()
    {
        // TODO: Implement getAliases() method.
        return [];
    }

    public function apply()
    {
        // TODO: Implement apply() method.
        $departmentData = [];
        $departmentData['name'] = "Resource Humain Managment";
        $departmentData['description'] = "This is they description of the department";
        $this->department->addData($departmentData);
        $this->department->getResource()->save($this->department);
    }

    public static function getVersion()
    {
        // TODO: Implement getVersion() method.
        return '2.0.0';
    }
}
