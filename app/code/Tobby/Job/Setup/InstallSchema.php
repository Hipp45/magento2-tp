<?php
//namespace Tobby\Helloworld\Setup;
//
//use Magento\Framework\DB\Ddl\Table;
//use Magento\Framework\Setup\InstallSchemaInterface;
//use Magento\Framework\Setup\ModuleContextInterface;
//use Magento\Framework\Setup\SchemaSetupInterface;
//
//class InstallSchema implements InstallSchemaInterface
//{
//    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
//    {
//        $installer = $setup;
//        $installer->startSetup();
//
//        /**
//         * Table tobby_department creation
//         */
//        $table_name = $installer->getTable('tobby_department');
//        if (!$installer->getConnection()->isTableExists($table_name)) {
//            $table = $installer->getConnection()->newTable($table_name)
//                ->addColumn('entity_id', Table::TYPE_INTEGER, null, ['identity'=>true, 'unsigned'=>true, 'nullable'=>false, 'primary'=>true], 'Contacts ID')
//                ->addColumn('name', Table::TYPE_TEXT, 255, ['nullable'=> false, 'default'=>''], 'Department name')
//                ->addColumn('description', Table::TYPE_TEXT, 2048, ['nullable'=> false, 'default'=> ''], 'Department description')
//                ->setComment('Department management for jobs module')
//                ->addIndex($installer->getIdxName('tobby_department', ['smallint', 'bigint']), ['smallint', 'bigint'])
//                ->setOption('type', 'InnoDB')
//                ->setOption('charset', 'utf8');
//            try {
//                $installer->getConnection()->createTable($table);
//            } catch (\Zend_Db_Exception $e) {
//            }
//        }
//        $installer->endSetup();
////        $tableName = $installer->getTable('tobby_departement');//Define table name
////        $tableComment = 'Department management for jobs module';
////        $colums = [
////            'entity_id' => [
////                'type' => Table::TYPE_INTEGER,
////                'size' => null,
////                'options' => [
////                    'identity' => true,
////                    'unsigned' => true,
////                    'nullable' => false,
////                    'primary' => true,
////                ],
////                'comment' => 'Department ID',
////            ],
////            'name' => [
////                'type' => Table::TABLE_TEXT,
////                'size' => 255,
////                'options' => [
////                    'nullable' => false,
////                    'default' => '',
////                ],
////                'comment' => 'Department name',
////            ],
////            'description' => [
////                'type' => Table::TABLE_TEXT,
////                'size' => 2048,
////                'options' => [
////                    'nullable' => false,
////                    'default' => '',
////                ],
////                'comment' => 'Department description',
////            ],
////        ];
////        //Define
////        $indexes = [];
////        $foreignKeys = [];
//    }
//}
