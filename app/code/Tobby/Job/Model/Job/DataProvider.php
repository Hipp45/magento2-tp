<?php
namespace Tobby\Job\Model\Job;

use Tobby\Job\Model\ResourceModel\Job\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $jobCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $jobCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $jobCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = [];
        /** @var Customer $customer */
        foreach ($items as $job) {
            // notre fieldset s'apelle "job" d'ou ce tableau pour que magento puisse retrouver ses datas :
            $this->loadedData[$job->getId()]['job'] = $job->getData();
        }

        return $this->loadedData;
    }
}
