<?php
namespace Tobby\Job\Model;

use Magento\Framework\Model\AbstractModel;

class Department extends AbstractModel
{
    const DEPARTMENT_ID = 'entity_id';

    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'job';
    /**
     * Name of the event object
     * @var string
     */
    protected $_eventObject = 'department';
    /**
     * Name of object is field
     * @var string
     */
    protected $_idFieldName = self::DEPARTMENT_ID;
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tobby\Job\Model\ResourceModel\Department');
    }
}
