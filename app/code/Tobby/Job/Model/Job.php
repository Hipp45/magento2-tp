<?php
namespace Tobby\Job\Model;

use Magento\Framework\Model\AbstractModel;

class Job extends AbstractModel
{
    const JOB_ID = 'job_id';

    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'job';
    /**
     * Name of the event object
     * @var string
     */
    protected $_eventObject = 'job';
    /**
     * Name of object is field
     * @var string
     */
    protected $_idFieldName = self::JOB_ID;
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tobby\Job\Model\ResourceModel\Job');
    }

    public function getEnableStatus()
    {
        return 1;
    }

    public function getDisableStatus()
    {
        return 0;
    }

    public function getAvailableStatuses()
    {
        return [$this->getDisableStatus() => __('Disabled'), $this->getEnableStatus() => __('Enabled')];
    }
}
