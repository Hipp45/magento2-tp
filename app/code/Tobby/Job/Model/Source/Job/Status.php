<?php
namespace Tobby\Job\Model\Source\Job;

use Tobby\Job\Model\Job;

class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var Job
     */
    protected $_job;

    /**
     * Constructor
     *
     * @param \Tobby\Job\Model\Job $job
     */
    public function __construct(\Tobby\Job\Model\Job $job)
    {
        $this->_job = $job;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_job->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
