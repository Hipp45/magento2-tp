<?php
namespace Tobby\Job\Model\Department;

use Tobby\Job\Model\ResourceModel\Department\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $departmentCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $departmentCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $departmentCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = [];
        /** @var Customer $customer */
        foreach ($items as $department) {
            // notre fieldset s'apelle "department" d'ou ce tableau pour que magento puisse retrouver ses datas :
            $this->loadedData[$department->getId()]['department'] = $department->getData();
        }

        return $this->loadedData;
    }
}
